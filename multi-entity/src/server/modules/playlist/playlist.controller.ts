import express, { Request, Response, NextFunction } from "express";
import * as playlist_service from "./playlist.service.js";
import raw from "./../../middleware/route.async.wrapper.js";
import * as fs from "fs";

const router = express.Router();
router.use(express.json());

function printToLog(req : Request, res : Response, next : NextFunction) {
    const method = req.method;
    const path = req.path;
    const time = Date.now();
    const content = `method: ${method}, path: ${path}, time: ${time} \n`;

    fs.writeFile("./logFile.txt", content, { flag: "a" }, err => {
    if (err) {
        console.error(err);
        return;
    }
    //file written successfully
    });

    next();
};

router.use(printToLog);

// GET ALL PLAYLISTS       
router.get("/"    , raw( async (req : Request, res : Response) => { 
  const playlists = await playlist_service.get_all_playlists();
  res.status(200).json(playlists);
}));

// CREATES A NEW PLAYLIST
router.post("/"    , raw (async (req : Request, res : Response) => {
     const playlist = await playlist_service.create_playlist(req.body);
     res.status(200).json(playlist);
}));

// GETS PLAYLIST BY ID
router.get("/:id" , raw( async (req : Request, res : Response) => {
  const playlist = await playlist_service.get_playlist_by_id(req.params.id);
  if (!playlist) return res.status(404).json({ status: "No playlist found." });
  res.status(200).json(playlist);
}));

// UPDATE PLAYLIST BY ID
router.put("/:id" , raw( async (req : Request, res : Response) => {
  const playlist = await playlist_service.update_playlist_by_id(req.params.id, req.body);
  res.status(200).json(playlist);
}));

// DELETE PLAYLIST BY ID
router.delete( "/:id" , raw( async (req : Request, res : Response) => {
  const playlist = await playlist_service.delete_playlist_by_id(req.params.id);
  if (!playlist) return res.status(404).json({ status: "No playlist found." });
  res.status(200).json(playlist);
}));


export default router;

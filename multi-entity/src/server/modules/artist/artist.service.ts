import log from "@ajar/marker";
import artist_model from "./artist.model.js";

export const get_all_artists = async () => {
  const artists = await artist_model.find();
  return artists;
};
export const create_artist = async (payload : any) => {
  const artist = await artist_model.create(payload);
  return artist;
};
export const get_artist_by_id = async (artist_id : string) => {
  const artist = await artist_model.findById(artist_id);//.populate("song");
  return artist;
};
export const update_artist_by_id = async (artist_id : string, payload : any) => {
  const artist = await artist_model.findByIdAndUpdate(artist_id, payload, {
    new: true,
    upsert: true,
  });//.populate("song");
  return artist;
};
export const delete_artist_by_id = async (artist_id : string) => {
    const artist = await artist_model.findByIdAndRemove(artist_id);
    return artist;
};
// export const get_artist_by_song_id = async (song_id : string) => {
//   console.log("line 28");
//   for await (const artist of artist_model.find([{ $sort: { name: 1 } }])) {
//     console.log("line 30");
//     for await (const song of artist.songs) {
//       console.log("line 32");
//       if(song._id === Number(song_id)) {
//         console.log("line 34");
//         return artist;
//       }
//     }
//   }
//   console.log("return without artist");
// };

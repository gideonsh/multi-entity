import express, { Request, Response, NextFunction } from "express";
import * as artist_service from "./artist.service.js";
import raw from "./../../middleware/route.async.wrapper.js";
import * as fs from "fs";

const router = express.Router();
router.use(express.json());

function printToLog(req : Request, res : Response, next : NextFunction) {
    const method = req.method;
    const path = req.path;
    const time = Date.now();
    const content = `method: ${method}, path: ${path}, time: ${time} \n`;

    fs.writeFile("./logFile.txt", content, { flag: "a" }, err => {
    if (err) {
        console.error(err);
        return;
    }
    //file written successfully
    });

    next();
};

router.use(printToLog);

// GET ALL ARTISTS       
router.get("/"    , raw( async (req : Request, res : Response) => { 
  const artists = await artist_service.get_all_artists();
  res.status(200).json(artists);
}));

// CREATES A NEW ARTIST
router.post("/"    , raw (async (req : Request, res : Response) => {
     const artist = await artist_service.create_artist(req.body);
     res.status(200).json(artist);
}));

// GETS ARTIST BY ID
router.get("/:id" , raw( async (req : Request, res : Response) => {
  const artist = await artist_service.get_artist_by_id(req.params.id);
  if (!artist) return res.status(404).json({ status: "No artist found." });
  res.status(200).json(artist);
}));

// UPDATE ARTIST BY ID
router.put("/:id" , raw( async (req : Request, res : Response) => {
  const artist = await artist_service.update_artist_by_id(req.params.id, req.body);
  res.status(200).json(artist);
}));

// DELETE ARTIST BY ID
router.delete( "/:id" , raw( async (req : Request, res : Response) => {
  const artist = await artist_service.delete_artist_by_id(req.params.id);
  if (!artist) return res.status(404).json({ status: "No artist found." });
  res.status(200).json(artist);
}));


export default router;
